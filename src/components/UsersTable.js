import { Button, Container, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { useEffect, useState } from "react";
import ModalAddUser from "./ModalAddUser"
import ModalEditUser from "./ModalEditUser"
import ModalDeleteUser from "./ModalDeleteUser"
function UsersTable (){
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };
    
    const [users, setUsers] = useState([]);
    const [varRefeshPage, setVarRefeshPage] = useState(0);

    const [openModalAdd, setOpenModalAdd] = useState(false);
    const [openModalEdit, setOpenModalEdit] = useState(false);
    const [openModalDelete, setOpenModalDelete] = useState(false);

    const [rowClicked, setRowClicked] = useState([])
    const [idDelete, setIdDelete] = useState("")

    const handleClose = () => setOpenModalAdd(false);
    const handleCloseEdit = () => setOpenModalEdit(false);
    const handleCloseDelete = () => setOpenModalDelete(false);

    const getData = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }
    const onBtnEditClick = (row) => {
        console.log("Nút sửa được click")
        console.log("ID: " + row.id)
        setOpenModalEdit(true)
        setRowClicked(row)
    }
    const onBtnDeleteClick = (row) => {
        console.log("Nút xóa được click")
        console.log("ID: " + row.id)
        setOpenModalDelete(true)
        setIdDelete(row.id)
    }
    const onBtnAddUserClick = () =>{
        console.log("Nút thêm được click")
        setOpenModalAdd(true)
    }
    useEffect(() => {
        getData("http://203.171.20.210:8080/crud-api/users/")
            .then((data) =>{
                console.log(data);
                setUsers(data);
                
            })
            .catch((error) =>{
                console.log(error);
            })
    }, [varRefeshPage])
    return (
        <Container>
            <Button onClick={onBtnAddUserClick} className="mb-2" value="add-user" style={{ borderRadius: 10, backgroundColor: "#689f38", padding: "10px 20px", fontSize: "10px"}}variant="contained">Thêm +</Button>
            <Grid container>
                <Grid item xs = {12} md = {12} lg = {12}>
                    <TableContainer component={Paper}>
                        <Table sx={{minWidth : 650}} aria-label="users table">
                            <TableHead>
                                <TableRow style={{backgroundColor: "#dcedc8"}}>
                                    <TableCell align="center">ID</TableCell>
                                    <TableCell align="center">Firstname</TableCell>
                                    <TableCell align="center">Lastname</TableCell>
                                    <TableCell align="center">Country</TableCell>
                                    <TableCell align="center">Customer type</TableCell>
                                    <TableCell align="center">Register status</TableCell>
                                    <TableCell align="center">Subject</TableCell>
                                    <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {users.map((row, index) => (
                                    <TableRow className="bg-light" key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                        <TableCell align="center">{row.id}</TableCell>
                                        <TableCell align="center">{row.firstname}</TableCell>
                                        <TableCell align="center">{row.lastname}</TableCell>
                                        <TableCell align="center">{row.country}</TableCell>
                                        <TableCell align="center">{row.customerType}</TableCell>
                                        <TableCell align="center">{row.registerStatus}</TableCell>
                                        <TableCell align="center">{row.subject}</TableCell>
                                        <TableCell align="center">
                                            <Button value={index} onClick={() => { onBtnEditClick(row) }} style={{ borderRadius: 25, backgroundColor: "#2196f3", padding: "10px 20px", fontSize: "10px"}}variant="contained">Sửa</Button>
                                            <Button value={index*index} onClick={() => { onBtnDeleteClick(row) }} style={{ borderRadius: 25, backgroundColor: "#f50057", padding: "10px 20px", fontSize: "10px"}}variant="contained">Xóa</Button>
                                        </TableCell>
                                    </TableRow>
                                ))}

                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <ModalAddUser varRefeshPage={varRefeshPage} setOpenModalAdd={setOpenModalAdd} openModalAdd={openModalAdd} handleClose={handleClose} style={style} getData={getData} setVarRefeshPage={setVarRefeshPage}/>
            <ModalEditUser varRefeshPage={varRefeshPage} openModalEdit={openModalEdit} handleCloseEdit={handleCloseEdit}
             style={style} getData={getData} setVarRefeshPage={setVarRefeshPage} rowClicked={rowClicked}
             />
            <ModalDeleteUser varRefeshPage={varRefeshPage} setVarRefeshPage={setVarRefeshPage} style={style} openModalDelete={openModalDelete} idDelete={idDelete} handleCloseDelete={handleCloseDelete}/>
        </Container>
    )
}

export default UsersTable