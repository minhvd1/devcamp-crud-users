import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography } from "@mui/material"
import { Col, Row } from "reactstrap"
import { useEffect, useState } from "react";

function ModalAddUser ({openModalAdd, setOpenModalAdd, handleClose, style, getData, setVarRefeshPage, varRefeshPage}) {
    const [firstnameForm, setFirstname] = useState("");
    const [lastnameForm, setLastname] = useState("");
    const [countryForm, setCountry] = useState("VN");
    const [registerStatusForm, setRegisterStatus] = useState("Accepted");
    const [subjectForm, setSubjectForm] = useState("");

    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [stutusModal, setStatusModal] = useState("error")

    const handleCloseAlert = () =>{
        setOpenAlert(false)
    }
    const onFirstnameChange = (event) =>{
        setFirstname(event.target.value)
    }
    const onLastnameChange = (event) =>{
        setLastname(event.target.value)
    }
    const onSelectCountryChange = (event) =>{
        setCountry(event.target.value)
    }
    const onSelectRegisterStatusChange = (event) =>{
        setRegisterStatus(event.target.value)
    }
    const onSubjectChange = (event) =>{
        setSubjectForm(event.target.value)
    }

    const onBtnInsertClick = () => {
        console.log("Insert được click!")
        var vDataForm = {
            firstname: firstnameForm,
            lastname: lastnameForm,
            country: countryForm,
            registerStatus: registerStatusForm,
            subject: subjectForm
            
        }
        var vCheckData = validateDataForm(vDataForm);
        if(vCheckData){
            console.log(vDataForm)
            const body = {
                method: 'POST',
                body: JSON.stringify({
                    firstname: vDataForm.firstname,
                    lastname: vDataForm.lastname,
                    country: vDataForm.country,
                    registerStatus: vDataForm.registerStatus,
                    subject: vDataForm.subject
                }),
                headers: {
                  'Content-type': 'application/json; charset=UTF-8',
                }
            }
            getData('http://203.171.20.210:8080/crud-api/users/', body)
            .then((data) =>{
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("Dữ liệu thêm thành công!")
                setOpenModalAdd(false)
                setVarRefeshPage(varRefeshPage + 1)
                console.log(data);
                //setUsers(data);
            })
            .catch((error) =>{
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Dữ liệu thêm thất bại!")
                console.log(error);
            })
        }
    }
    
    const validateDataForm = (paramDataForm) => {
        if(paramDataForm.firstname === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Firstname phải được điền vào!");

            return false;
        }
        if(paramDataForm.lastname === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Lastname phải được điền vào!");
            return false;
        }
        if(paramDataForm.subject === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Subject phải được điền vào!");
            return false;
        }
        else{
            return true;
        }
    }
    const onBtnCancelClick = () => {
        handleClose()
    }
    return(
        <>
        <Modal
            open={openModalAdd}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            >
            <Box sx={style}>
                <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                    <strong>Thêm User!</strong>
                </Typography>
                    <Row>
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Firstname:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onFirstnameChange}/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                
                    <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Lastname:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onLastnameChange}/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Country:</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                            id="country-select"
                                            defaultValue="VN"
                                            fullWidth
                                            onChange={onSelectCountryChange}
                                        >
                                            <MenuItem value="VN">Việt Nam</MenuItem>
                                            <MenuItem value="USA">USA</MenuItem>
                                            <MenuItem value="AUS">Australia</MenuItem>
                                            <MenuItem value="CAN">Canada</MenuItem>
                                    </Select>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Register Status:</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                        id="registerstatus-select"
                                        defaultValue="Accepted"
                                        fullWidth
                                        onChange={onSelectRegisterStatusChange}
                                    >
                                        <MenuItem value="Accepted">Accepted</MenuItem>
                                        <MenuItem value="Denied">Denied</MenuItem>
                                        <MenuItem value="Standard">Standard</MenuItem>
                                    </Select>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Subject:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onSubjectChange}/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4">
                                <Col sm="6">
                                    <Button onClick={onBtnInsertClick} className="bg-success w-75 text-white">Insert User</Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onBtnCancelClick} className="bg-success w-75 text-white">Hủy Bỏ</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
            </Box>
        </Modal>
        <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity={stutusModal} sx={{ width: '100%' }}>
            {noidungAlertValid}
            </Alert>
        </Snackbar>
      </>
    )
}
export default ModalAddUser