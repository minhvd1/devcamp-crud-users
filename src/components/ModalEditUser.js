import { Alert, Box, Button, Input, MenuItem, Modal, Select, Snackbar, Typography } from "@mui/material"
import { Col, Row } from "reactstrap"
import { useEffect, useState } from "react";

function ModalEditUser ({openModalEdit, handleCloseEdit, style, getData, setVarRefeshPage, varRefeshPage, rowClicked
}) {
    const [openAlert, setOpenAlert] = useState(false)
    const [statusModalEdit, setStatusModalEdit] = useState("error")
    const [noidungAlertValid, setNoidungAlertValid] = useState("")

    const [idEdit, setIdEdit] = useState("");
    const [firstnameEditForm, setFirstnameEditForm] = useState("");
    const [lastnameEditForm, setLastnameEditForm] = useState("");
    const [countryEditForm, setCountryEditForm] = useState("");
    const [customerTypeEditForm, setCustomerTypeEditForm] = useState("");
    const [registerStatusEditForm, setRegisterStatusEditForm] = useState("");
    const [subjectEditForm, setSubjectEditForm] = useState("");

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const onBtnCancelClick = () => {
        handleCloseEdit()
    }
    const onBtnUpdateClick = () => {
        console.log("Update được click!")
        var vCheckData = validateDataForm()
        if (vCheckData){
            const body = {
                method: 'PUT',
                body: JSON.stringify({
                    firstname: firstnameEditForm,
                    lastname: lastnameEditForm,
                    country: countryEditForm,
                    customerType: customerTypeEditForm,
                    registerStatus: registerStatusEditForm,
                    subject: subjectEditForm
                }),
                headers: {
                  'Content-type': 'application/json; charset=UTF-8',
                },
            }
    
            getData('http://203.171.20.210:8080/crud-api/users/' + idEdit, body)
                .then((data) => {
                    console.log(data);
                    setOpenAlert(true);
                    setNoidungAlertValid("Update User thành công!")
                    setStatusModalEdit("success")
                    setVarRefeshPage(varRefeshPage + 1)
                    handleCloseEdit()
                })
                .catch((error) => {
                    console.log(error);
                    setOpenAlert(true);
                    setNoidungAlertValid("Update User thất bại!")
                    setStatusModalEdit("error")
                    handleCloseEdit()
                })
        }
    }
    
    const onFirstnameChange = (event) => {
        setFirstnameEditForm(event.target.value)
    }
    const onLastnameChange = (event) => {
        setLastnameEditForm(event.target.value)
    }
    const onSelectCountryChange = (event) => {
        setCountryEditForm(event.target.value)
    }
    const onSelectCustomerTypeChange = (event) => {
        setCustomerTypeEditForm(event.target.value)
    }
    const onSelectRegisterStatusChange = (event) => {
        setRegisterStatusEditForm(event.target.value)
    }
    const onSubjectChange = (event) => {
        setSubjectEditForm(event.target.value)
    }
    const validateDataForm = () => {
        if(firstnameEditForm === ""){
            setOpenAlert(true);
            setNoidungAlertValid("Firstname chưa được điền!")
            setStatusModalEdit("error")
            return false
        }
        if(lastnameEditForm === ""){
            setOpenAlert(true);
            setNoidungAlertValid("Lastname chưa được điền!")
            setStatusModalEdit("error")
            return false
        }
        if(!(countryEditForm === "VN" || countryEditForm === "USA" || countryEditForm === "AUS" || countryEditForm === "CAN") || countryEditForm === null){
            setOpenAlert(true);
            setNoidungAlertValid("Country Cần chọn lại cho đúng!")
            setStatusModalEdit("error")
            return false
        }
        if(!(customerTypeEditForm === "Standard" || customerTypeEditForm === "Gold" || customerTypeEditForm === "Premium") || customerTypeEditForm === null){
            setOpenAlert(true);
            setNoidungAlertValid("Customer Type cần chọn lại cho đúng!")
            setStatusModalEdit("error")
            return false
        }
        if(!(registerStatusEditForm === "Standard" || registerStatusEditForm === "Accepted" || registerStatusEditForm === "Denied") || customerTypeEditForm === null){
            setOpenAlert(true);
            setNoidungAlertValid("Register Status cần chọn lại cho đúng!")
            setStatusModalEdit("error")
            return false
        }
        if(subjectEditForm === ""){
            setOpenAlert(true);
            setNoidungAlertValid("Subject chưa được điền!")
            setStatusModalEdit("error")
            return false
        }
        else {
            return true
        }
    }
    useEffect(() => {
        setIdEdit(rowClicked.id)
        setFirstnameEditForm(rowClicked.firstname)
        setLastnameEditForm(rowClicked.lastname)
        setCountryEditForm(rowClicked.country)
        setCustomerTypeEditForm(rowClicked.customerType)
        setRegisterStatusEditForm(rowClicked.registerStatus)
        setSubjectEditForm(rowClicked.subject)
    }, [openModalEdit])
    return(
        <>
        <Modal
            open={openModalEdit}
            onClose={onBtnCancelClick}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            >
            <Box sx={style}>
                <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                    <strong>Edit User!</strong>
                </Typography>
                    <Row>
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>ID:</label>
                                </Col>
                                <Col sm="7">
                                    <Input readOnly value={idEdit} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Firstname:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onFirstnameChange}  value={firstnameEditForm}/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                
                    <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Lastname:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onLastnameChange} value={lastnameEditForm}/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Country:</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                            id="country-select"
                                            value={countryEditForm ? countryEditForm : ""}
                                            defaultValue= {countryEditForm ? countryEditForm : ""}
                                            fullWidth
                                            onChange={onSelectCountryChange}
                                        >
                                            <MenuItem value="VN">Việt Nam</MenuItem>
                                            <MenuItem value="USA">USA</MenuItem>
                                            <MenuItem value="AUS">Australia</MenuItem>
                                            <MenuItem value="CAN">Canada</MenuItem>
                                    </Select>
                                    
                                    {!(countryEditForm === "VN" || countryEditForm === "USA" || countryEditForm === "AUS" || countryEditForm === "CAN") ? <i style={{fontSize: "10px", color: "red"}}>Country không nằm trong select dữ liệu!</i> : null }
                                    
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Customer Type:</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                            id="customertype-select"
                                            value={customerTypeEditForm ? customerTypeEditForm : ""}
                                            defaultValue={customerTypeEditForm ? customerTypeEditForm : ""}
                                            fullWidth
                                            onChange={onSelectCustomerTypeChange}
                                        >
                                            <MenuItem value="Standard">Standard</MenuItem>
                                            <MenuItem value="Gold">Gold</MenuItem>
                                            <MenuItem value="Premium">Premium</MenuItem>
                                    </Select>
                                    {!(customerTypeEditForm === "Standard" || customerTypeEditForm === "Gold" || customerTypeEditForm === "Premium") ? <i style={{fontSize: "10px", color: "red"}}>Customer Type không nằm trong select dữ liệu!</i> : null}
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Register Status:</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                        id="registerstatus-select"
                                        value={registerStatusEditForm ? registerStatusEditForm : ""}
                                        defaultValue= {registerStatusEditForm ? registerStatusEditForm : ""}
                                        fullWidth
                                        onChange={onSelectRegisterStatusChange}
                                    >
                                        <MenuItem value="Accepted">Accepted</MenuItem>
                                        <MenuItem value="Denied">Denied</MenuItem>
                                        <MenuItem value="Standard">Standard</MenuItem>
                                    </Select>
                                    {!(registerStatusEditForm === "Accepted" || registerStatusEditForm === "Denied" || registerStatusEditForm === "Standard") ? <i style={{fontSize: "10px", color: "red"}}>Register Status không nằm trong select dữ liệu!</i> : null }
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Subject:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onSubjectChange} value={subjectEditForm}/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4">
                                <Col sm="6">
                                    <Button onClick={onBtnUpdateClick} className="bg-success w-100 text-white">Update User</Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onBtnCancelClick} className="bg-success w-75 text-white">Hủy Bỏ</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
            </Box>
        </Modal>
        <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity={statusModalEdit} sx={{ width: '100%' }}>
            {noidungAlertValid}
            </Alert>
      </Snackbar>
      </>
    )
}
export default ModalEditUser