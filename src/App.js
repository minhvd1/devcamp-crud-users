import 'bootstrap/dist/css/bootstrap.min.css';
import { Label } from 'reactstrap';
import UsersTable from "./components/UsersTable"
function App() {
  return (
    <div>
      <div style={{textAlign: "center"}}>
        <Label className='mt-4'><h2>Danh sách người dùng đăng ký</h2></Label>
      </div>
      <UsersTable/>
    </div>
  );
}

export default App;
